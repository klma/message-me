﻿using message_me.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace message_me.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        public MainWindowViewModel()
        {
            ConnectionManager = new ConnectionManager();
            ConnectionManager.NewMessageReceived += ConnectionManager_NewMessageReceived;
            ConnectionManager.StatusChanged += ConnectionManager_StatusChanged;
        }
     
        public ConnectionManager ConnectionManager { get; set; }

        private string sentMessage;
        public string SentMessage
        {
            get { return sentMessage; }
            set { sentMessage = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// MessagesCollection: contains list of all received & sent messages.
        /// </summary>
        public ObservableCollection<string> MessageCollection { get; set; } = new ObservableCollection<string>();

        /// <summary>
        /// Status: Represent info that being shown on StatusBar
        /// </summary>
        public ObservableCollection<string> Status { get; set; } = new ObservableCollection<string>();

        private void ConnectionManager_StatusChanged(object sender, EventArgs e)
        {
            Status.Clear();
            Status.Add(sender.ToString());
        }

        private void ConnectionManager_NewMessageReceived(object sender, EventArgs e)
        {
            string receivedMessage = sender.ToString();
            if (String.IsNullOrEmpty(receivedMessage))
                return;
            string msg = string.Format("server: {0}", receivedMessage);
            MessageCollection.Add(msg);
            receivedMessage = string.Empty;
        }

        public void SendMessage()
        {
            if (String.IsNullOrEmpty(SentMessage))
                return;
                        
            string msg = string.Format("me: {0}", SentMessage);
            MessageCollection.Add(msg);
            ConnectionManager.SendMessage(SentMessage);
            SentMessage = string.Empty;            
        }
    }
}
