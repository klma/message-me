﻿using message_me.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace message_me.Models
{
    public class ConnectionManager
    {
        public string ServerIp { get; set; } = "127.0.0.1";
        public int Port { get; set; } = 5566;

        /// <summary>
        /// public event fires when new message comes from server.
        /// </summary>
        public event EventHandler NewMessageReceived;

        /// <summary>
        /// Fires on every change of connection status
        /// </summary>
        public event EventHandler StatusChanged;

        public void SendMessage(string message)
        {
            try
            {
                TcpClient tcpClient = new TcpClient();            
                if (StatusChanged != null)
                    StatusChanged("Connecting.....", null);

                // connect to server
                tcpClient.Connect(ServerIp, Port);
                if (StatusChanged != null)
                    StatusChanged("Connected", null);
                Stream stream = tcpClient.GetStream();

                ASCIIEncoding asen = new ASCIIEncoding();
                byte[] ba = asen.GetBytes(message);
                if (StatusChanged != null)
                    StatusChanged("Transmitting.....", null);
                stream.Write(ba, 0, ba.Length);

                byte[] bb = new byte[100];
                int k = stream.Read(bb, 0, 100);
                string serverMsg = "";
                for (int i = 0; i < k; i++)
                    serverMsg += Convert.ToChar(bb[i]);

                if (NewMessageReceived != null && !string.IsNullOrEmpty(serverMsg))
                { 
                    NewMessageReceived(serverMsg, new EventArgs());
                    if (StatusChanged != null)
                        StatusChanged("Message Received", null);
                }
                tcpClient.Close();
            }

            catch (Exception e)
            {
                if (StatusChanged != null)
                    StatusChanged("Error.....", null);
                MessageBox.Show("Error..... " + e.StackTrace);                
            }
        }
    }
}
