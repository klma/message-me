﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace server_console
{
    public class ListenerManager
    {
        public ListenerManager()
        {
            IPAddress ipaddress = IPAddress.Parse(ServerIp);
            TcpListener = new TcpListener(ipaddress, 5566);
        }

        public TcpListener TcpListener { get; private set; }

        // IP address of server all clients should use to connect
        public string ServerIp { get; set; } = "127.0.0.1";

        public void Go()
        {           
            try
            {
                // Start listening to comming connection from clients. 
                TcpListener.Start();                    
                /* 
                 * Infinity loop that accept coming connections, but in real application must take into consideration a server load & equipment we have. 
                 * we must accept certain type of connection by reading first part of message then anlyise it
                 * to know if belongs to us or not
                 */
                while (true)
                {
                    Console.WriteLine();
                    Program.DM.ShowLogs();
                    Console.WriteLine();
                    Console.WriteLine("The server is running at local end point " + TcpListener.LocalEndpoint);
                    Console.WriteLine("Waiting for comming connection....");
                    Console.WriteLine();
                    Socket socket = TcpListener.AcceptSocket();
                    Console.WriteLine("Connection accepted from " + socket.RemoteEndPoint);

                    /* reading client message */
                   string clientMsg =  ReadMessage(socket);

                    /* Send confirmation to client that message has been received successfully */
                    SendConfirmation(socket, clientMsg);

                    /* clean up */
                    socket.Close();
                    //tcpListener.Stop();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error..... " + e.StackTrace);
                TcpListener.Stop();
            }
        }

        public string ReadMessage(Socket socket)
        {
            if (socket == null || !socket.Connected)
                return "";
            byte[] b = new byte[255];
            int size = socket.Receive(b);
            if (size == 0)
                return "";

            string clientMsg = "";
            for (int i = 0; i < size; i++)
                clientMsg += Convert.ToChar(b[i]);
            Console.WriteLine(String.Format("Received message: {0}", clientMsg));

            // Log message
            Program.DM.LogMessage(clientMsg, socket.RemoteEndPoint.ToString());
            return clientMsg;
        }

        public void SendConfirmation(Socket socket, string clientMsg)
        {
            if (socket == null || !socket.Connected)
                return;

            ASCIIEncoding asen = new ASCIIEncoding();
            int substrLength = 0;
            substrLength = clientMsg.Length >= 10 ? 10 : clientMsg.Length;
            socket.Send(asen.GetBytes(
                String.Format("'{0}...' received successfully", clientMsg.Substring(0, substrLength))
                ));
            Console.WriteLine("Confirmation has been sent to client");
        }     
    }
}
