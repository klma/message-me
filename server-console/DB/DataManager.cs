﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server_console.DB
{
    /// <summary>
    /// DataManager: Represent db manager to create db, insert data & read data.
    /// </summary>
    public class DataManager
    {
        public DataManager()
        {
            CreateDB();                               
        }

        private string connectionString = "Data Source=MessageMeDB.sqlite;Version=3;";
        private string dbName = "MessageMeDB.sqlite";
        public void CreateDB()
        {
            if (!File.Exists(dbName))
            {
                Console.WriteLine("Create MessageMeDB.sqlite for first time...");
                SQLiteConnection.CreateFile(dbName);

                using (SQLiteConnection dbConnection = new SQLiteConnection(connectionString))
                {
                    dbConnection.Open();
                    Console.WriteLine("Creating MessageLog table...");
                    string sql = " create table messagelog ( id INTEGER PRIMARY KEY, message varchar(100), clientip varchar(20)) ";
                    SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
                    command.ExecuteNonQuery();                   
                    dbConnection.Close();
                }
            }
        }

        /// <summary>
        /// LogMessage: save received message to database
        /// </summary>
        /// <param name="message">client message to be saved</param>
        /// <param name="cleintIp">client ip rceiving message from</param>
        public void LogMessage(string message, string cleintIp)
        {
            using (SQLiteConnection dbConnection = new SQLiteConnection(connectionString))
            {
                Console.WriteLine(" insert client message to messagelog table ");
                dbConnection.Open();
                string sql = string.Format(" insert into messagelog (message, clientip) values ('{0}', '{1}') ", message, cleintIp);
                SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
                command.ExecuteNonQuery();
                dbConnection.Close();
            }
        }

        /// <summary>
        /// ShowLogs: Read all messages logs from messagelog table
        /// </summary>
        public void ShowLogs()
        {
            using (SQLiteConnection dbConnection = new SQLiteConnection(connectionString))
            {
                Console.WriteLine("reading messagelog table.....");
                dbConnection.Open();
                SQLiteCommand command = new SQLiteCommand(" Select * FROM messagelog ORDER BY id DESC LIMIT 3 ", dbConnection);
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Display the all client messages with ips
                    Console.WriteLine("messagelog last 3 messages:");
                    while (reader.Read())
                    {
                        Console.WriteLine(reader["message"] + " : " + reader["clientip"]);    
                    }
                    dbConnection.Close();
                }
            }
        }
    }
}
