﻿using server_console.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace server_console
{
    public class Program
    {
        public static ListenerManager ListenerManager;
        public static DataManager DM;
        static void Main(string[] args)
        {
            ListenerManager = new ListenerManager();
            DM = new DataManager();
            ListenerManager.Go();
        }

    }
    
}
